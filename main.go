package main

import (
	"annotate/backend/config"
	"annotate/backend/state"
	_ "embed"
	"fmt"
	"log"

	"github.com/wailsapp/wails"
)

//go:embed frontend/public/index.html
var html string

//go:embed frontend/build/static/js/main.js
var js string

//go:embed frontend/build/static/css/main.css
var css string

func main() {
	_config := config.NewConfig()

	oldState, err := state.OldState()

	if err != nil {
		log.Fatal(err)
	}

	app := wails.CreateApp(&wails.AppConfig{
		Width:            oldState.Width,
		Height:           oldState.Height,
		MinWidth:         _config.MinWidth,
		MinHeight:        _config.MinHeight,
		Title:            _config.TitleBarName,
		HTML:             html,
		JS:               js,
		CSS:              css,
		Colour:           "#131313",
		Resizable:        true,
		DisableInspector: true,
	})

	app.Bind(&_config)
	app.Bind(&state.StateManager{})

	err = app.Run()
	if err != nil {
		fmt.Println(err)
		log.Fatal(err)
	}

}
