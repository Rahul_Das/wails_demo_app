package utility

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
)

const (
	AppName      = "Annotate"
	CacheFile    = "cache.json"
	Version      = "v0.0.1"
	TitleBarName = "Annotate"
	MinWidth     = 800
	MinHeight    = 600
)

func EnsureDir(dirName string) (bool, error) {
	info, error := os.Stat(dirName)
	if error != nil && os.IsNotExist(error) {
		if error := os.Mkdir(dirName, os.ModeDir); error != nil {
			return true, error
		}
		return true, nil
	} else if info != nil && info.IsDir() {
		return false, nil
	} else if info != nil && !info.IsDir() {
		return false, errors.New("path exists but is not a directory")
	} else {
		return true, error
	}
}

func EnsureFile(fileName string) (bool, error) {
	info, error := os.Stat(fileName)
	if error != nil && os.IsNotExist(error) {
		file, error := os.Create(fileName)
		if error != nil {
			return true, error
		}
		file.Close()
		return true, nil
	} else if info != nil && info.IsDir() {
		return false, errors.New("path exists but is a directory")
	} else if info != nil && !info.IsDir() {
		return false, nil
	} else {
		return true, error
	}
}

func GetAppDir() (string, error) {
	homeDir, error := os.UserHomeDir()
	if error != nil {
		return "", error
	}

	appdirectory := filepath.Join(homeDir, AppName)

	if _, error := EnsureDir(appdirectory); error != nil {
		return "", error
	}

	return appdirectory, nil
}

func GetCachePath() (string, error) {
	appdirectory, error := GetAppDir()
	if error != nil {
		return "", error
	}
	return filepath.Join(appdirectory, CacheFile), nil
}

func ReadFromJson(jsonPath string, structure interface{}) error {
	byteValue, err := ioutil.ReadFile(jsonPath)
	if err != nil {
		fmt.Println(err)
		return err
	}
	jsonBytes := []byte(byteValue)
	err = json.Unmarshal(jsonBytes, structure)
	return err
}

func WriteToJson(jsonPath string, structure interface{}) error {
	file, err := json.MarshalIndent(structure, "", " ")
	if err != nil {
		fmt.Println(err)
		return err
	}
	err = ioutil.WriteFile(jsonPath, file, 0644)
	return err
}
