package utility

import (
	"testing"
)

func TestReadFromJson(t *testing.T) {
	type state struct {
		Left   int
		Top    int
		Height int
		Width  int
	}
	s1 := &state{}
	err := ReadFromJson("C:/Users/admin/Annotate/cache.json", s1)
	if err != nil {
		t.Errorf("File is not Valid")
	}
	if s1.Height != 600 && s1.Width != 800 && s1.Top != 0 && s1.Left != 0 {
		t.Errorf("File is not Valid")
	}
	s2 := &state{}
	err = ReadFromJson("C:/Users/admin/Annotate/cache1.json", s2)
	if err == nil {
		t.Errorf("File is not Valid")
	}

}

func TestWriteToJson(t *testing.T) {
	type state struct {
		Left   int
		Top    int
		Height int
		Width  int
	}
	s1 := &state{Left: 0, Top: 0, Height: 600, Width: 800}
	err := WriteToJson("../../temp/cache.json", s1)
	if err != nil {
		t.Errorf("File is not Valid")
	}
	s2 := &state{}
	err = ReadFromJson("../../temp/cache.json", s2)
	if err != nil {
		t.Errorf("File is not Valid")
	}
	if s2.Height != 600 && s2.Width != 800 && s2.Top != 0 && s2.Left != 0 {
		t.Errorf("File is not Valid")
	}
}
