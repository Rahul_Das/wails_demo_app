package state

import (
	"annotate/backend/utility"
	"os"

	"github.com/wailsapp/wails"
)

type StateManager struct {
	state     WindowState
	cachePath string
}

func (m *StateManager) WailsInit(runtime *wails.Runtime) error {
	cachePath, err := utility.GetCachePath()
	if err != nil {
		return err
	}
	m.cachePath = cachePath
	m.state, err = OldState()
	if err != nil {
		return err
	}
	return nil
}

func (m *StateManager) UpdateState(width int, height int) (WindowState, error) {
	m.state.Width = width
	m.state.Height = height
	return m.state, nil
}

func (m *StateManager) reload() error {
	info, error := os.Stat(m.cachePath)
	if error != nil && os.IsNotExist(error) {
		m.state = defaultState()
		return nil
	} else if info != nil && !info.IsDir() {
		err := utility.ReadFromJson(m.cachePath, &m.state)
		return err
	} else {
		return error
	}
}

func (m *StateManager) save() error {
	_, err := utility.EnsureFile(m.cachePath)
	if err != nil {
		return err
	}
	err = utility.WriteToJson(m.cachePath, &m.state)
	return err
}

func (m *StateManager) WailsShutdown() {
	m.save()
}
