package state

import (
	"annotate/backend/utility"
	"os"
)

type WindowState struct {
	Left   int `json:"left"`
	Top    int `json:"top"`
	Height int `json:"height"`
	Width  int `json:"width"`
}

func defaultState() WindowState {
	state := WindowState{}
	state.Left = 0
	state.Top = 0
	state.Height = 600
	state.Width = 800
	return state
}

func OldState() (WindowState, error) {
	cachePath, err := utility.GetCachePath()
	if err != nil {
		return WindowState{}, err
	}
	info, err := os.Stat(cachePath)
	if err != nil && os.IsNotExist(err) {
		state := defaultState()
		return state, nil
	} else if info != nil && !info.IsDir() {
		state := WindowState{}
		err := utility.ReadFromJson(cachePath, &state)
		return state, err
	} else {
		state := defaultState()
		return state, nil
	}
}
