package config

import (
	"annotate/backend/utility"
)

type Config struct {
	AppName      string
	TitleBarName string
	Version      string
	AppDir       string
	CacheFile    string
	MinHeight    int
	MinWidth     int
}

func NewConfig() Config {

	config := Config{}
	config.AppName = utility.AppName
	config.Version = utility.Version
	config.TitleBarName = utility.TitleBarName
	config.MinHeight = utility.MinHeight
	config.MinWidth = utility.MinWidth

	return config
}
