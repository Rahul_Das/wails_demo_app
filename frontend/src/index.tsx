import React from 'react';
import ReactDOM from 'react-dom';
import 'core-js/stable';
import './index.css';
import App from './App';
import {IBackend} from './typedef/backend'

import * as Wails from '@wailsapp/runtime';

/* 
export interface CustomWindow extends Window {
  backend: IBackend;
}
declare var window: CustomWindow;
 */
declare global {
  interface Window {
    backend: IBackend;
  }
}

Wails.Init(() => {
  ReactDOM.render(
    <React.StrictMode>
      <App />
    </React.StrictMode>,
    document.getElementById("app")
  );
});
