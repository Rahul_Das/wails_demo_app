import React , { useLayoutEffect, useState }  from 'react'
import './App.css'

function App() {
  const [size, setSize] = useState([0, 0]);
  useLayoutEffect(() => {
    const updateSize = () => {
      // console.log(window.outerWidth, window.outerHeight)
      window.backend.StateManager.UpdateState(window.innerWidth, window.innerHeight)
      .then((state: any)=> {
        setSize([window.innerWidth, window.innerHeight]);
        console.log(state.left)
      }).catch((e: Error)=> console.log('This Is error'))
    }
    window.addEventListener('resize', updateSize);
    updateSize();
    return () => window.removeEventListener('resize', updateSize);
  }, [])

  return (
    <div className="App">
      <div>Wails React App</div>
    </div>
  )
}

export default App
