export interface IState{
  left: number
	top : number
	height : number
	width: number
}

export interface IStateManager{
  UpdateState: (width: number, height: number)=> Promise<IState> 
}

export interface IBackend{
  StateManager:IStateManager
}

